import { css } from '@emotion/core'

export default css`
  .app--wrapper {
    margin: 0 auto;
    max-width: 1024px;
    position: relative;
    min-height: 100vh;
    width: 90%;
    z-index: 1;
  }
`
