import React from 'react'
import CouponForm from 'UI/organisms/CouponForm'
import { HomePageContainer } from './styles'

const HomePage = () => (
  <HomePageContainer>
    <header>a</header>
    <CouponForm />
    <footer>b</footer>
  </HomePageContainer>
)

export default HomePage
