import styled from '@emotion/styled'
import { device } from 'config/utils'
export const CouponFooterContainer = styled.div`
  display: flex;
  flex-direction: column;
  @media ${device.laptop} {
    flex-direction: row;
  }
`
