import React from 'react'
import { CouponFooterContainer } from './styles'
import CustomButtonContainer from 'UI/atoms/Button'
const CouponButonWrapper = () => {
  // tipo,color, tamaño de letra, curso pointer
  return (
    <CouponFooterContainer>
      <CustomButtonContainer
        sucess
        medium
        colorPrimary
        whiteText
        sucessBorderColor
      >
        Crear Cupón
      </CustomButtonContainer>
      <CustomButtonContainer inverse medium blackText inverseBorderColor>
        Actualizar
      </CustomButtonContainer>
      <CustomButtonContainer inverted medium blackText disabled>
        cancelar
      </CustomButtonContainer>
      <CustomButtonContainer sucess medium blackText disabled>
        cancelar
      </CustomButtonContainer>
    </CouponFooterContainer>
  )
}
export default CouponButonWrapper
