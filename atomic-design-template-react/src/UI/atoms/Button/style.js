import styled from '@emotion/styled'
import { css } from '@emotion/core'
import styledMap from 'styled-map'
const buttonTextColor = styledMap`
  sucessText: #00e3c2;
  warningText: #c00;
  inverse: #ff0055;
  info: #0cc;
  default: #ccc;
  whiteText:white;
  blackText: black;
`
const cursor = styledMap`
  disabled: not-allowed;
  default: pointer;
`
const borderColor = styledMap`
  sucessBorderColor: 1b9481;
  warning: #c00;
  inverseBorderColor: #ff0055;
  info: #0cc;
  default: #ccc;
`
const fontSize = styledMap`
  large: 32px;
  small: 8px;
  medium: 1.1rem;
  default: 1rem;
`
const successStyles = css`
  background-color: #00e3c2;
  &:hover {
    background-color: #1b9481;
    -webkit-box-shadow: 0 2px 2px 0px rgba(0, 0, 0, 0.25);
    box-shadow: 0 2px 2px 0px rgba(0, 0, 0, 0.25);
    color: white;
    border: none;
  }
  &:active{
    box-shadow: none;
  }
`
const invertedButtonStyles = css`
  background-color: white;
  &:hover {
    background-color: black;
    -webkit-box-shadow: 0 2px 2px 0px rgba(0, 0, 0, 0.25);
    box-shadow: 0 2px 2px 0px rgba(0, 0, 0, 0.25);
    color: white;
    border: none;
  }
`
const inverseButtonStyles = css`
  background-color: #ff0055;
  &:hover {
    background-color: #c00;
    -webkit-box-shadow: 0 2px 2px 0px rgba(0, 0, 0, 0.25);
    box-shadow: 0 2px 2px 0px rgba(0, 0, 0, 0.25);
    color: white;
    border: none;
  }
`
const getButtonStyles = props => {
  if (props.sucess) {
    return successStyles
  }
  if (props.inverted) {
    return invertedButtonStyles
  }
  if (props.inverse) {
    return inverseButtonStyles
  }
}

export const CustomButtonContainer = styled.button`
  width: auto;
  min-width: 190px;
  border-radius: 45px;
  line-height: 50px;
  text-transform: uppercase;
  font-family: 'Open Sans Condensed';
  font-weight: bolder;
  cursor: ${cursor};
  display: flex;
  justify-content: center;
  color: ${buttonTextColor};
  font-size:${fontSize};
  border: 2px solid ${buttonTextColor};
  border-color:${borderColor};
  ${getButtonStyles}
  &:active{
    box-shadow: none;
  }
`
