import styled from '@emotion/styled'

export const CouponFormContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const CouponFormTitle = styled.h1`
  padding: 2rem;
  font-size: 2em;
  margin: 0 auto;
`
