import React from 'react'

import CouponButonWrapper from 'UI/molecules/CouponButonWrapper'
import { CouponFormContainer, CouponFormTitle } from './styles'
const CouponForm = () => {
  return (
    <CouponFormContainer>
      <CouponFormTitle>Cupones</CouponFormTitle>
      <CouponButonWrapper />
    </CouponFormContainer>
  )
}
export default CouponForm
