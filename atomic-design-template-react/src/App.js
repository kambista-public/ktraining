import React from 'react'
import { Global, css } from '@emotion/core'
import globals from 'config/globals'
import HomePage from 'components/pages/Home'
const App = () => {
  return (
    <>
      <Global
        styles={css`
        ${globals}
      `}
      />
      <div className='app--wrapper'>
        <HomePage />
      </div>
    </>
  )
}

export default App
